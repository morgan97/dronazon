package DRONE;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.25.0)",
    comments = "Source: delivery.proto")
public final class DeliveryServiceGrpc {

  private DeliveryServiceGrpc() {}

  public static final String SERVICE_NAME = "DRONE.DeliveryService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<DRONE.Delivery.Message,
      DRONE.Delivery.Message> getDeliveryMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "delivery",
      requestType = DRONE.Delivery.Message.class,
      responseType = DRONE.Delivery.Message.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<DRONE.Delivery.Message,
      DRONE.Delivery.Message> getDeliveryMethod() {
    io.grpc.MethodDescriptor<DRONE.Delivery.Message, DRONE.Delivery.Message> getDeliveryMethod;
    if ((getDeliveryMethod = DeliveryServiceGrpc.getDeliveryMethod) == null) {
      synchronized (DeliveryServiceGrpc.class) {
        if ((getDeliveryMethod = DeliveryServiceGrpc.getDeliveryMethod) == null) {
          DeliveryServiceGrpc.getDeliveryMethod = getDeliveryMethod =
              io.grpc.MethodDescriptor.<DRONE.Delivery.Message, DRONE.Delivery.Message>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "delivery"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DRONE.Delivery.Message.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DRONE.Delivery.Message.getDefaultInstance()))
              .setSchemaDescriptor(new DeliveryServiceMethodDescriptorSupplier("delivery"))
              .build();
        }
      }
    }
    return getDeliveryMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DeliveryServiceStub newStub(io.grpc.Channel channel) {
    return new DeliveryServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DeliveryServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DeliveryServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DeliveryServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DeliveryServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DeliveryServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<DRONE.Delivery.Message> delivery(
        io.grpc.stub.StreamObserver<DRONE.Delivery.Message> responseObserver) {
      return asyncUnimplementedStreamingCall(getDeliveryMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDeliveryMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                DRONE.Delivery.Message,
                DRONE.Delivery.Message>(
                  this, METHODID_DELIVERY)))
          .build();
    }
  }

  /**
   */
  public static final class DeliveryServiceStub extends io.grpc.stub.AbstractStub<DeliveryServiceStub> {
    private DeliveryServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DeliveryServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DeliveryServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DeliveryServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<DRONE.Delivery.Message> delivery(
        io.grpc.stub.StreamObserver<DRONE.Delivery.Message> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getDeliveryMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class DeliveryServiceBlockingStub extends io.grpc.stub.AbstractStub<DeliveryServiceBlockingStub> {
    private DeliveryServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DeliveryServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DeliveryServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DeliveryServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   */
  public static final class DeliveryServiceFutureStub extends io.grpc.stub.AbstractStub<DeliveryServiceFutureStub> {
    private DeliveryServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DeliveryServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DeliveryServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DeliveryServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_DELIVERY = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DeliveryServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DeliveryServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DELIVERY:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.delivery(
              (io.grpc.stub.StreamObserver<DRONE.Delivery.Message>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DeliveryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DeliveryServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return DRONE.Delivery.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DeliveryService");
    }
  }

  private static final class DeliveryServiceFileDescriptorSupplier
      extends DeliveryServiceBaseDescriptorSupplier {
    DeliveryServiceFileDescriptorSupplier() {}
  }

  private static final class DeliveryServiceMethodDescriptorSupplier
      extends DeliveryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DeliveryServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DeliveryServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DeliveryServiceFileDescriptorSupplier())
              .addMethod(getDeliveryMethod())
              .build();
        }
      }
    }
    return result;
  }
}
