package DRONE;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.25.0)",
    comments = "Source: delivery.proto")
public final class RechargeServiceGrpc {

  private RechargeServiceGrpc() {}

  public static final String SERVICE_NAME = "DRONE.RechargeService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<DRONE.Delivery.Message,
      DRONE.Delivery.Message> getRechargeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "recharge",
      requestType = DRONE.Delivery.Message.class,
      responseType = DRONE.Delivery.Message.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<DRONE.Delivery.Message,
      DRONE.Delivery.Message> getRechargeMethod() {
    io.grpc.MethodDescriptor<DRONE.Delivery.Message, DRONE.Delivery.Message> getRechargeMethod;
    if ((getRechargeMethod = RechargeServiceGrpc.getRechargeMethod) == null) {
      synchronized (RechargeServiceGrpc.class) {
        if ((getRechargeMethod = RechargeServiceGrpc.getRechargeMethod) == null) {
          RechargeServiceGrpc.getRechargeMethod = getRechargeMethod =
              io.grpc.MethodDescriptor.<DRONE.Delivery.Message, DRONE.Delivery.Message>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "recharge"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DRONE.Delivery.Message.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DRONE.Delivery.Message.getDefaultInstance()))
              .setSchemaDescriptor(new RechargeServiceMethodDescriptorSupplier("recharge"))
              .build();
        }
      }
    }
    return getRechargeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static RechargeServiceStub newStub(io.grpc.Channel channel) {
    return new RechargeServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static RechargeServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new RechargeServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static RechargeServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new RechargeServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class RechargeServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<DRONE.Delivery.Message> recharge(
        io.grpc.stub.StreamObserver<DRONE.Delivery.Message> responseObserver) {
      return asyncUnimplementedStreamingCall(getRechargeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getRechargeMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                DRONE.Delivery.Message,
                DRONE.Delivery.Message>(
                  this, METHODID_RECHARGE)))
          .build();
    }
  }

  /**
   */
  public static final class RechargeServiceStub extends io.grpc.stub.AbstractStub<RechargeServiceStub> {
    private RechargeServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RechargeServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RechargeServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RechargeServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<DRONE.Delivery.Message> recharge(
        io.grpc.stub.StreamObserver<DRONE.Delivery.Message> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getRechargeMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class RechargeServiceBlockingStub extends io.grpc.stub.AbstractStub<RechargeServiceBlockingStub> {
    private RechargeServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RechargeServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RechargeServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RechargeServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   */
  public static final class RechargeServiceFutureStub extends io.grpc.stub.AbstractStub<RechargeServiceFutureStub> {
    private RechargeServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RechargeServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RechargeServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RechargeServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_RECHARGE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final RechargeServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(RechargeServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_RECHARGE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.recharge(
              (io.grpc.stub.StreamObserver<DRONE.Delivery.Message>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class RechargeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    RechargeServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return DRONE.Delivery.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("RechargeService");
    }
  }

  private static final class RechargeServiceFileDescriptorSupplier
      extends RechargeServiceBaseDescriptorSupplier {
    RechargeServiceFileDescriptorSupplier() {}
  }

  private static final class RechargeServiceMethodDescriptorSupplier
      extends RechargeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    RechargeServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (RechargeServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new RechargeServiceFileDescriptorSupplier())
              .addMethod(getRechargeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
