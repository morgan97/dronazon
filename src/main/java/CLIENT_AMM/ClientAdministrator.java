package CLIENT_AMM;

// the administrator client connects to the server for checking the status of the network and get the statistics

import BEANS.Drone;
import BEANS.StatisticAverage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ClientAdministrator {

    private static Client client;

    public static void main(String args[]) throws IOException {
        // starting listening the server

        client = Client.create();
        String ip = "localhost";
        int portServer = 6789;
        String serverAddressAdministrator = "http://" + ip + ":" + portServer;


        // server requests
        String askDrones = "/drone/list";
        String askAverages = "/statistics/list";
        String askLastNStatistics = "/statistics/lastN=/";
        String askStatisticFromAnAmountOfTime = "/statistics/fromTime=/";
        String askAverageBetweenTwoTimeStamp = "/statistics/averageTwoTimestamp/";

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // loop
        boolean cont = true;
        label:
        while (cont) {

            // user instructions
            System.out.println("------------------------------------------------------------");
            System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
            System.out.println("0 -> close the service");
            System.out.println("1 -> list all the drones present in the ring");
            System.out.println("2 -> get the last N statistics");
            System.out.println("3 -> get the mean of deliveries between two timestamp");
            System.out.println("4 -> get the mean of kilometers between two timestamp");
            System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
            System.out.println("------------------------------------------------------------");


            String query = null;
            try {
                query = br.readLine();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            int value = Integer.parseInt(query);
            switch (value) {
                case 0:
                    cont = false;
                    break label;
                case 1:
                    listDrones(serverAddressAdministrator + askDrones);
                    break;
                case 2:
                    lastNStat(serverAddressAdministrator + askLastNStatistics, br);
                    break;
                case 3:
                    betweenDeliveries(serverAddressAdministrator + askAverageBetweenTwoTimeStamp, br, true);
                    break;
                case 4:
                    betweenDeliveries(serverAddressAdministrator + askAverageBetweenTwoTimeStamp, br, false);
                    break;
                default:
                    break;
            }
        }

        client.destroy();

    }


    //////////////////////////// DRONE LIST ////////////////////////////
    private static void listDrones(String url) {
        ClientResponse clientResponse = getRequest(client, url);
        if (clientResponse.getStatus() == 200) {

            System.out.println(">>> List of Drones <<<");

            String listDrones = clientResponse.getEntity(String.class);
            Type listOfMyClassObject = new TypeToken<ArrayList<Drone>>() {
            }.getType();
            Gson gson = new Gson();
            List<Drone> list = gson.fromJson(listDrones, listOfMyClassObject);


            if (list.size() == 0 || list == null) {
                System.out.println("No drones are working actually");
            } else {
                for (int i = 0; i < list.size(); i++) {
                    System.out.println(list.get(i));
                }
            }
        }

    }


    //////////////////////////// LAST N STATISTICS ////////////////////
    private static void lastNStat(String url, BufferedReader bufferedReader) throws IOException {
        System.out.println("Number of statistics?");
        String value = bufferedReader.readLine();
        int val = Integer.parseInt(value);
        if (val < 0) {
            System.out.println("Number not valid, retry");
            return;
        }

        ClientResponse clientResponse = getRequest(client, url + val);
        if (clientResponse.getStatus() == 200) {

            System.out.println(">>> Last " + val + " averages <<<");

            String listAverages = clientResponse.getEntity(String.class);
            Type listOfMyClassObject = new TypeToken<ArrayList<StatisticAverage>>() {
            }.getType();
            Gson gson = new Gson();
            List<StatisticAverage> list = gson.fromJson(listAverages, listOfMyClassObject);

            if (list.size() == 0 || list == null) {
                System.out.println("No Averages found");
            } else {
                for (int i = 0; i < list.size(); i++) {
                    StatisticAverage element = list.get(i);
                    System.out.print("{ ");
                    System.out.print("    Timestamp = " + element.getTimestamp());
                    System.out.print("    Battery = " + element.getBattery());
                    System.out.print("    Deliveries = " + element.getDeliveries());
                    System.out.print("    Kilometers = " + element.getKm());
                    System.out.print("    Pollution = " + element.getPollution());
                    System.out.println("     }");
                }
            }

        } else if (clientResponse.getStatus() == 400) {
            System.out.println("the value entered is not correct, enter a new number");
        }

    }

    private static void betweenDeliveries(String url, BufferedReader bufferedReader, boolean check) throws IOException {
        String value;
        System.out.println("from ? ");
        value = bufferedReader.readLine();
        long val1 = Long.parseLong(value);
        System.out.println("to ? ");
        value = bufferedReader.readLine();
        long val2 = Long.parseLong(value);

        ClientResponse clientResponse = getRequest(client, url + val1 + "/" + val2);
        if (clientResponse.getStatus() == 200) {

            System.out.println(">>> Statistic between " + val1 + " and " + val2 + "<<<");

            String listAverages = clientResponse.getEntity(String.class);
            Type listOfMyClassObject = new TypeToken<ArrayList<StatisticAverage>>() {
            }.getType();
            Gson gson = new Gson();
            List<StatisticAverage> list = gson.fromJson(listAverages, listOfMyClassObject);

            if (list.size() == 0 || list == null) {
                System.out.println("No Averages found");
            } else {
                computeList(list, check);
            }
        } else if (clientResponse.getStatus() == 400) {
            System.out.println("the values entered are not correct (>0 and <Long.MAX_SIZE)");
        }
    }


    private static ClientResponse getRequest(Client client, String url) {
        WebResource webResource = client.resource(url);

        try {
            return webResource.get(ClientResponse.class);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void computeList(List<StatisticAverage> list, boolean check) {
        float mean = 0;

        // print deliveries or kilometers
        if (check == true) {
            for (int i = 0; i < list.size(); i++) {
                mean += list.get(i).getDeliveries();
            }
            System.out.println("The Mean of the Deliveries is " + mean / list.size());
        } else {
            for (int i = 0; i < list.size(); i++) {
                mean += list.get(i).getKm();
            }
            System.out.println("The Mean of the Kilometers is " + mean / list.size());
        }
    }
}
