package SERVER_AMM.services;

import BEANS.Drone;
import BEANS.Drones;
import UTILS.Utils;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("drone")
public class droneInterface {

    // request of insertion of a drone
    @Path("add")
    @POST
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public Response addDrone(Drone drone){
        // check if a drone with the same id already exists
        if (!Drones.getInstance().checkDrone(drone)){

            // if true, the ID is never used -> proceed

            // set posX and posY for the drone
            drone.setPosX(Utils.generatePos());
            drone.setPosY(Utils.generatePos());

            // add the drone to the main list of drones
            Drones.getInstance().add(drone);
            String output = new Gson().toJson(Drones.getInstance().getDroneslist());
            System.out.println(output);
            System.out.println("------------------------------------------");
            return Response.ok(output).build();
        } else {
            // if false, the id is already in use -> retry
            return Response.status(501).build();
        }
    }

    // request of exit of a drone
    @Path("delete")
    @DELETE
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public Response deleteDrone(Drone drone){
        if (Drones.getInstance().checkDrone(drone)){
            // deleting the drone
            boolean deleteCorrectly = Drones.getInstance().delete(drone);
            Response manage = manage(deleteCorrectly);
            return manage;
        } else {
            return Response.status(404).build();
        }
    }

    // request of delete from a drone who noticed that another drone have abandoned the ring (not controlled)
    @Path("deleteId=/{id}")
    @DELETE
    @Produces({"application/json", "application/xml"})
    public Response deleteDroneId(@PathParam("id") int id){
        // deleting the drone
        System.out.println("entrato");
        boolean deleteCorrectly = Drones.getInstance().deleteId(id);
        return manage(deleteCorrectly);
    }


    @Path("list")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response listDrones(){
        String output = new Gson().toJson(Drones.getInstance().getDroneslist());
        return Response.ok(output).build();
    }

    public Response manage(boolean deleteCorrectly){
        if (deleteCorrectly){
            String output = new Gson().toJson(Drones.getInstance().getDroneslist());
            System.out.println(output);
            System.out.println("------------------------------------------");
            return Response.ok(output).build();
        } else {
            return Response.status(409).build();
        }
    }

}


