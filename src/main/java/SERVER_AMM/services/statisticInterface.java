package SERVER_AMM.services;

import BEANS.StatisticAverage;
import BEANS.StatisticAverageCollection;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("statistics")
public class statisticInterface {

    // add a new average measurement
    @Path("add")
    @POST
    @Consumes({"application/json", "application/xml"})
    public Response addStatistic(StatisticAverage statisticAverage) {
        StatisticAverageCollection.getInstance().add(statisticAverage);
        return Response.ok().build();
    }

    // return the list of all the averages
    @Path("list")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response list() {
        System.out.println("Requested all the statistics");

        List<StatisticAverage> averageList = StatisticAverageCollection.getInstance().getAverageList();
        String output = new Gson().toJson(averageList);
        return Response.ok(output).build();
    }

    // return all the statistics from a particular amount of time
    @Path("fromTime=/{time}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response fromTimeX(@PathParam("time") Long t) {
        System.out.println("Looking for data from time = " + t);

        List<StatisticAverage> averageList = StatisticAverageCollection.getInstance().getAverageList();
        List<StatisticAverage> listToReturn = new ArrayList<>();
        for (int i = 0; i < averageList.size(); i++) {
            StatisticAverage s = averageList.get(i);
            if (s.getTimestamp() >= t) {
                listToReturn.add(s);
            }
        }

        String output = new Gson().toJson(listToReturn);
        return Response.ok(output).build();
    }

    // return the last N statistics
    @Path("lastN=/{value}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response lastN_statistics(@PathParam("value") int val) {
        System.out.println("List of the last 'N' statistics");
        if (val >= 0) {
            List<StatisticAverage> averageList = StatisticAverageCollection.getInstance().getAverageList();
            List<StatisticAverage> listToReturn = new ArrayList<>();
            for (int i = averageList.size() - 1; i >= 0 && val > 0; i--, val--) {
                listToReturn.add(averageList.get(i));
            }

            String output = new Gson().toJson(listToReturn);
            return Response.ok(output).build();
        } else {
            return Response.status(400).build();
        }
    }

    // return mean between two measurements
    @Path("averageTwoTimestamp/{value1}/{value2}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response averageBetweenTwoValues(@PathParam("value1") long t1, @PathParam("value2") long t2) {
        System.out.println("number of the deliveries between two timestamp");
        boolean correct = assertionOnInput(t1, t2);
        if (correct) {
            List<StatisticAverage> averageList = StatisticAverageCollection.getInstance().getAverageList();
            List<StatisticAverage> listToReturn = new ArrayList<>();
            for (int i = 0; i < averageList.size(); i++) {
                StatisticAverage s = averageList.get(i);
                if (s.getTimestamp() >= t1 && s.getTimestamp() <= t2)
                    listToReturn.add(s);
            }

            String output = new Gson().toJson(listToReturn);
            return Response.ok(output).build();
        } else {
            return Response.status(400).build();
        }
    }


    // check input
    // if t1/t2 > 0 and t1/t2 < Long.MAX_VALUE -> true
    // else -> false
    private boolean assertionOnInput(long t1, long t2) {
        boolean test = true;
        if (t1 < 0 || t2 < 0)
            test = false;
        if (t1 > Long.MAX_VALUE || t2 > Long.MAX_VALUE)
            test = false;

        return test;
    }


}
