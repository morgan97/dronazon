package DRONE;

/*
 // some bibliography used
 https://www.vinsguru.com/grpc-client-streaming-api-in-java/
*/

import BEANS.*;
import DRONE.GraphicInterface.GuiSwingThread;
import DRONE.Mqtt.ListenerMqtt;
import DRONE.Mqtt.QueueMqtt;
import DRONE.StreamObservers.OutputStreamObserverRing;
import SIMULATORI.BufferImpl;
import SIMULATORI.Measurement;
import SIMULATORI.PM10Simulator;
import UTILS.Utils;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GrpcClient extends Thread {
    // attributes for this drone
    private final Drone_rpc drone_rpc;
    private final int id;
    private final String ip;
    private int portNextDrone;
    private int myPort;
    private long timestamp;
    private static List<MapIdPort> listDronesMapped;
    private static List<MasterDroneInternalStructure> masterDroneInternalStructures = null;
    private Queue q;
    private QueueMqtt queueMqtt = null;
    private static final Object lockInternalStructureMaster = new Object();
    private ListenerMqtt listenerMqtt;
    private boolean im_Master = false;
    private boolean participant = false;
    private int master = -1;
    private int posX;
    private int posY;

    // attributes for the ring connection
    private ManagedChannel channel;
    private DeliveryServiceGrpc.DeliveryServiceStub clientStub;
    private StreamObserver<Delivery.Message> inputStreamObserver;

    // attributes for the direct connection
    private List<ChannelsDirect> channelDirects = new ArrayList<>();

    // attributes for the timeout
    private Timeout timeoutEnteringInTheRing;
    private Timeout timeoutExitingFromTheRing;

    // attributes for the sensors
    private BufferImpl bufferSensorsPM10;
    private PM10Simulator pm10Simulator;

    // attributes to print
    private int battery = 100;
    private int totDeliveries = 0;
    private int totKilometers = 0;
    private int totalKmFromLastDeliverySaved = 0;
    private List<Measurement> measurementList = new ArrayList<>();
    private PrinterStatistics printerStatistics;

    // attributes for the keyboard listener
    private ListenerKeyboard listenerKeyboard;

    // attributes for the GUI
    private GuiSwingThread threadSwingInterface;

    // attributes for the logic
    private final Object lockExit = new Object();
    private volatile boolean working = true;

    // attributes for the logic/debugging of the application
    private Logger logger;
    private int counterMessages = 0;

    // attributes for charging
    private enum StateForRecharge {
        FREE,
        WANTED
    }

    private StateForRecharge stateForRecharge = StateForRecharge.FREE;
    private long timestampForRecharge = Long.MAX_VALUE;
    private java.util.Queue localQueueForRecharge = new LinkedList<Delivery.Message>();
    private List<MapIdBoolean> mapIdBooleanDronesForRecharge = new ArrayList<>();

    // attributes for detecting the exit of a drone
    private enum StateOfVigilance {
        CALM,
        ALERT
    }

    private StateOfVigilance stateOfVigilance = StateOfVigilance.CALM;

    // attributes for the controlled exit of a drone
    private enum StateForExit {
        FREE,
        WANTED
    }

    private StateForExit stateForExit = StateForExit.FREE;
    private long timestampForExit = Long.MAX_VALUE;
    private java.util.Queue localQueueForExit = new LinkedList<Delivery.Message>();
    private List<MapIdBoolean> mapIdBooleanDronesForExit = new ArrayList<>();

    public enum EnterExit {
        ENTERING,
        EXITING
    }

    // Constructor
    GrpcClient(Drone_rpc drone_rpc, int id, String ip, int portNextDrone, int myPort, Queue q, long timestamp, List<MapIdPort> listDronesMapped, Logger logger, int posX, int posY) {
        this.drone_rpc = drone_rpc;
        this.id = id;
        this.ip = ip;
        this.portNextDrone = portNextDrone;
        this.myPort = myPort;
        this.timestamp = timestamp;
        this.listDronesMapped = listDronesMapped;
        this.q = q;
        this.logger = logger;
        this.posX = posX;
        this.posY = posY;
    }

    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /******************************************************ENGINE**************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    @Override
    public void run() {
        System.out.println(" ---- Start Client ---- ");

        setupDirect();      // setup direct connections
        setupRing();        // setup ring connection

        // setup timeout
        timeoutEnteringInTheRing = new Timeout(this, id, channelDirects, EnterExit.ENTERING);
        timeoutEnteringInTheRing.start();

        // -> topology message to update the ring
        Delivery.Message.Builder update = MessageCreator.createMessageTopology(id, timestamp, listDronesMapped, false);
        inputStreamObserver.onNext(update.build());

        // start the GUI
        threadSwingInterface = new GuiSwingThread(q);
        threadSwingInterface.run();
        threadSwingInterface.reloadInfoGUI(id, battery, posX, posY, im_Master, masterDroneInternalStructures);

        // when the drone correctly entered the ring, let's start the PM reader
        // sensors for the pollution PM10
        bufferSensorsPM10 = new BufferImpl(String.valueOf(id));
        pm10Simulator = new PM10Simulator(bufferSensorsPM10);
        pm10Simulator.start();

        // start the printer
        printerStatistics = new PrinterStatistics(this, threadSwingInterface);
        printerStatistics.start();

        // start listener
        listenerKeyboard = new ListenerKeyboard(drone_rpc, id, q);
        listenerKeyboard.start();

        /*******************************************************CORE***************************************************************/
        label:
        while (working) {
            Delivery.Message message = q.take();
            // wait for debugging

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (message != null)
                manageSwitcherMessage(message);
            else
                manageNullMessage();

            // reload the GUI
            threadSwingInterface.reloadInfoGUI(id, battery, posX, posY, im_Master, masterDroneInternalStructures);
        }
        /*************************************************************************************************************************/

        // shutdown all the channels
        channelShutdownDirect();
        channelShutdownRing();

        Utils.printClosingThread("CLIENT");
        logger.log(Level.WARNING, "Closing client...");
    }

    private void manageSwitcherMessage(Delivery.Message message) {

        // if we receive a message in the ring it means that drone behind is present or a response (type == 11)
        // then reset the state of vigilance
        if ((message.getType() >= 0 && message.getType() <= 7) || (message.getType() >= 11 && message.getType() <= 12))
            setStateOfVigilance();

        System.out.println("///// MESSAGE ID = " + message.getIdMessage() + " Opening -> " + printTime(System.currentTimeMillis()) + " /////");
        // assert message!=null;
        // -> the message is about the topology
        switch (message.getType()) {
            case 0:
                topologyLogic(message);
                break;
            case 1:
                questionForTheMasterLogic(message);
                break;
            case 2:
                electionInProgressLogic(message);
                break;
            case 3:
                roundOfSignaturesLogic(message);
                break;
            case 4:
                electedDroneLogic(message);
                break;
            case 5:
                infoForTheMasterLogic(message);
                break;
            case 6:
                newPackageLogic(message);
                break;
            case 7:
                ackDeliveryLogic(message);
                break;
            case 8:
                manageQuestionOfRecharge(message);
                break;
            case 9:
                manageAnswerOfRecharge(message);
            case 10:
                manageAreYouOkFunc(message);
                break;
            case 11:
                manageImOkFunc(message);
                break;
            case 12:
                resetMasterDroneInternalStructure(message);
                break;
            case 13:
                manageQuestionOfExit(message);
                break;
            case 14:
                manageAnswerOfExit(message);
                break;
            case 15:
                managePendingOrders(message);
                break;

            default:
                break;
        }
    }

    private void manageNullMessage() {

        // if there is just one drone is not a problem
        if (listDronesMapped.size() == 1)
            return;

        // if it is the first timeout, send a message back to the drone
        // to discover if it is present
        if (stateOfVigilance == StateOfVigilance.CALM) {
            stateOfVigilance = StateOfVigilance.ALERT;
            System.out.println("ENTERING IN STATE OF VIGILANCE");

            Delivery.Message.Builder askIfTheDroneIsOk = MessageCreator.createAreYouOkMessageQuestion(id);

            int idBehind = Utils.findPredecessorId(id, listDronesMapped);
            channelDirects.forEach(element -> {
                if (element.getId() == idBehind)
                    element.sendMessage(askIfTheDroneIsOk.build());
            });
            return;
        }

        // if stateOfVigilance is already on ALERT it means that the drone has not received the message back
        // so it deletes the drone who has contacted from the list and create a new topology in the ring
        if (stateOfVigilance == StateOfVigilance.ALERT) {
            int idBehind = Utils.findPredecessorId(id, listDronesMapped);

            List<MapIdPort> newListDroneMapped = buildNewListDroneMapped(idBehind);

            if (listDronesMapped.size() > 2) {
                // inputStreamObserver.onNext(update.build()); -> to update in a ring
                channelDirects.stream().parallel().forEach(element -> {
                    long time = System.currentTimeMillis();
                    Delivery.Message.Builder update = MessageCreator.createMessageTopology(element.getId(), time, newListDroneMapped, false);
                    element.sendMessage(update.build());

                });
            } else {    // enter here only if listDronesMapped.size == 2
                Delivery.Message.Builder update = MessageCreator.createMessageTopology(id, System.currentTimeMillis(), newListDroneMapped, false);
                q.put(update.build());
            }

            // master -> remove from master list
            printSubLevelMessage("Removing drone id = " + idBehind + " from master list");
            if (im_Master) {
                removeDroneFromMasterList(idBehind);
                for (MasterDroneInternalStructure m : masterDroneInternalStructures)
                    setStatusDrone(m.getId(), true);
            } else {
                // message for resetting the master drone internal structure of the master
                Delivery.Message.Builder resetMsg = MessageCreator.createResetMasterDroneInternalStructureMessage(master);
                inputStreamObserver.onNext(resetMsg.build());
            }

            // -> remove from server
            Utils.removeDroneFromServer(idBehind);
        }
    }

    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> LOGIC <- /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /************************************************************************************************************************topology**/
    private void topologyLogic(Delivery.Message message) {
        printMessageReceived("message type = 0 -> topology");
                /*
                    > compare our topology to the topology in the message
                    > if the timestamp of this drone is behind, then update
                    > else create a new message with an updated timestamp
                */

        if (message.getTopologyMessage().getId() == -1) {
            // recompileListDroneMapped(message);       // -> this is cause of a bug when two drones ask to exit -> we enter two times in grpcStopDrone
            // create new list

            // start the timeout for exit
            timeoutExitingFromTheRing = new Timeout(this, id, channelDirects, EnterExit.EXITING);
            timeoutExitingFromTheRing.start();

            List<MapIdPort> newListToPropagate = new ArrayList<>();
            List<Delivery.TupleIdPort> arrayList = message.getTopologyMessage().getArrayList();
            for (Delivery.TupleIdPort element : arrayList)
                newListToPropagate.add(new MapIdPort(element.getId(), element.getPort()));

            // propagate the new list
            Delivery.Message.Builder msg = MessageCreator.createMessageTopology(id,
                    message.getTopologyMessage().getTimestamp(),
                    newListToPropagate,
                    true);
            inputStreamObserver.onNext(msg.build());
            return;
        }

        // -> closing message arrived
        if (message.getTopologyMessage().getNeedClosing() == true) {
            if (message.getTopologyMessage().getId() == id) {
                printSubLevelMessage(". Forced closing ");

                // stop timeout for exiting
                printSubLowLevelMessage("Stopping Timeout for exiting from the ring");
                timeoutExitingFromTheRing.notifyThread();

                // free resource if there are for exit
                while (localQueueForExit.size() > 0) {
                    Delivery.Message savedMessage = (Delivery.Message) localQueueForExit.poll();
                    printSubLevelMessage("Free the resource for drone id = " + savedMessage.getExitMessage().getId());
                    composeAnswerExit(savedMessage);
                }
                stateForExit = StateForExit.FREE;
                timestampForExit = Long.MAX_VALUE;
                mapIdBooleanDronesForExit.clear();

                // release all the packages in the MqttQueue
                if (im_Master && queueMqtt != null) {
                    printSubLevelMessage("Assigning the pending orders");
                    List<Order> ordersToAssign = new ArrayList<>();
                    for (Order order : queueMqtt.getAllTheDeliveriesPending()) {
                        // -> put a message in the main queue for the master to deliver a new package
                        ordersToAssign.add(order);
                        printSubLowLevelMessage("order pending with id = " + order.getIdPackage());
                    }

                    if (ordersToAssign.size() > 0) {
                        Delivery.Message.Builder messageDeliveryOrdersPending = MessageCreator.createMessageDeliveryOrdersPending(ordersToAssign);
                        inputStreamObserver.onNext(messageDeliveryOrdersPending.build());
                    }
                }

                working = false;
                synchronized (lockExit) {
                    lockExit.notify();
                }
            } else {
                printSubLevelMessage("Message for exit, not for me");
                inputStreamObserver.onNext(message);
                changePorts(message);
                if (!im_Master && !participant) {
                    printSubLevelMessage("Message to ask who is the master...");
                    Delivery.Message.Builder findMaster = MessageCreator.createMessageElectionWhoIsMaster(id, -1);
                    inputStreamObserver.onNext(findMaster.build());
                }

                // this condition is given from a drone trying to exit while another try to get inside
                // search myself in the list
                boolean found = false;
                for (MapIdPort element : listDronesMapped) {
                    if (element.getId() == id)
                        found = true;
                }

                // if the drone is not found then recompute
                if (!found){
                    printSubLevelMessage("Entered condition of MYSELF NOTFOUND");
                    listDronesMapped.add(new MapIdPort(id, myPort));
                    Utils.sortMapListDronesById(listDronesMapped);

                    Delivery.Message.Builder newTop = MessageCreator.createMessageTopology(id, System.currentTimeMillis(), listDronesMapped, false);
                    inputStreamObserver.onNext(newTop.build());
                }
        }
    } else

    {
        changePorts(message);
        if (message.getTopologyMessage().getId() != id) {
            inputStreamObserver.onNext(message);
        } else {

            printSubLevelMessage("Message received, topology shared");
            printSubLowLevelMessage("Stopping Timeout for entering in the ring");
            timeoutEnteringInTheRing.notifyThread();

            if (!im_Master && !participant) {
                printSubLevelMessage("Message to ask who is the master...");
                Delivery.Message.Builder findMaster = MessageCreator.createMessageElectionWhoIsMaster(id, -1);
                inputStreamObserver.onNext(findMaster.build());
            }
        }
    }

}

    /************************************************************************************************************************askWhoisMaster**/
    private void questionForTheMasterLogic(Delivery.Message message) {
        printMessageReceived("message type 1 -> question for the master");

        // the question for finding the master arrived back
        if (message.getFindMaster().getId() == id) {
            // the master has been found
            if (message.getFindMaster().getMaster() != -1) {
                im_Master = false;
                // the master is changed
                if (message.getFindMaster().getMaster() != master) {
                    master = message.getFindMaster().getMaster();
                    printSubLevelMessage("master found / new master id = " + master);
                    // send message to update the master
                    Delivery.Message.Builder messageUpdateDroneMaster = MessageCreator.createMessageBatteryPositionStatus(id,
                            battery,
                            posX,
                            posY,
                            master,
                            totKilometers,
                            totDeliveries);
                    inputStreamObserver.onNext(messageUpdateDroneMaster.build());
                    printSubLevelMessage("sending info to the master");
                } else {    // the master is not changed
                    printSubLevelMessage("master found / the master is not changed");
                }

            } else { // the master has not been found, start new election
                printSubLevelMessage("master not found, start new election");
                // mark this drone "participant"
                participant = true;
                Delivery.Message.Builder messageElectionStartElection = MessageCreator.createMessageElectionStartElection(id, battery);
                inputStreamObserver.onNext(messageElectionStartElection.build());
            }
            return;
        }

        // the message is from another drone
        if (message.getFindMaster().getId() != id) {
            // -> if i'm master, replace it in the message
            if (im_Master) {
                printSubLevelMessage("Drone with id = " + message.getFindMaster().getId() + " asked who is the master, I'm the master");
                Delivery.Message.Builder messageElectionWhoIsMaster = MessageCreator.createMessageElectionWhoIsMaster(message.getFindMaster().getId(), id);
                inputStreamObserver.onNext(messageElectionWhoIsMaster.build());
            } else {    // -> i'm not master -> propagate the message
                inputStreamObserver.onNext(message);
            }
            return;
        }
    }

    /************************************************************************************************************************election**/
    private void electionInProgressLogic(Delivery.Message message) {
        printMessageReceived("message type 2 -> election in progress");
    /*
                    If the arrived identifier battery is greater, then it:
                    - If it is not a participant, it:
                        » Marks itself as a participant
                    — Forwards the message to its neighbor

                    If the arrived identifier battery is smaller:
                    — If it is not a participant, it:
                        » Marks itself as a participant
                        » Substitutes its own identifier in the election  message and sends it on
                    — If it is already a participant, it does nothing

                    If the arrived identifier is the id of this drone then:
                    - it is the coordinator, it marks itself as non participant and
                      send the message of elected to the others
                */

        // computing the drone to elect
        if (message.getElection().getId() != id) {

            // case nr 1 -> incoming message battery level major
            if (message.getElection().getBattery() > battery) {
                printSubLevelMessage("Battery level inferior -> propagate");
                if (!participant)
                    participant = true;
                inputStreamObserver.onNext(message);
            }
            // case nr 2 -> incoming message has same battery level -> check id
            else if (message.getElection().getBattery() == battery) {
                printSubLevelMessage("Same level of battery, choosing the major id");

                if (message.getElection().getId() > id) {
                    printSubLevelMessage("Id level inferior -> propagate");
                    if (!participant)
                        participant = true;
                    inputStreamObserver.onNext(message);
                } else {
                    if (!participant) {
                        printSubLevelMessage("id level major -> new message, candidate id = " + id);
                        participant = true;
                        Delivery.Message.Builder msg = MessageCreator.createMessageElectionStartElection(id, battery);
                        inputStreamObserver.onNext(msg.build());
                    } else {
                        printSubLevelMessage("Already participant with major id level");
                    }
                }
            }
            // case nr 3 -> incoming message has battery level inferior
            else {
                if (!participant) {
                    printSubLevelMessage("Battery level major -> new message, candidate id = " + id);
                    participant = true;
                    Delivery.Message.Builder msg = MessageCreator.createMessageElectionStartElection(id, battery);
                    inputStreamObserver.onNext(msg.build());
                } else {
                    printSubLevelMessage("Already participant with major battery level");
                }
            }
            return;
        }


        // the message arrived back, all the drones agree to elect the drone with id = message.getElection().getId() -> start petition
        if (message.getElection().getId() == id) {
            printSubLevelMessage("I WANT TO BECOME THE NEW COORDINATOR, STARTED ROUND OF SIGNATURES");

            // -> start petition
            Delivery.Message.Builder messagePetition = MessageCreator.createMessageRoundOfSignatures(id, new ArrayList<>());
            inputStreamObserver.onNext(messagePetition.build());
            return;
        }
    }

    private void roundOfSignaturesLogic(Delivery.Message message) {
        printMessageReceived("message type 3 -> Round of Signatures");

    /*
        when a drone want to become the coordinator, it sends a message in the ring
        if all the drone are agree, the drone can become the master, else, it asks again who is the master
     */

        List<Boolean> signatures = message.getRoundOfSignatures().getSignaturesList();
        if (message.getRoundOfSignatures().getId() == id) {
            boolean allAgree = checkSignatures(signatures);

            // check i'm not already the master and all drones are agree with the election
            if (im_Master) {
                printSubLevelMessage("I'm already the master");
                return;
            }
            if (!allAgree) {
                printSubLevelMessage("Someone not agree, asking who is the master");
                // -> ask who is the master
                Delivery.Message.Builder findMaster = MessageCreator.createMessageElectionWhoIsMaster(id, -1);
                inputStreamObserver.onNext(findMaster.build());
                return;
            }

            printSubLevelMessage("Everyone agree with the election");
            // -> I'm the new coordinator
            im_Master = true;
            master = id;
            // -> init arraylist
            masterDroneInternalStructures = new ArrayList<>();
            // -> announce i'm the master
            Delivery.Message.Builder messageElectionElected = MessageCreator.createMessageElectionElected(id);
            inputStreamObserver.onNext(messageElectionElected.build());

        } else {
            // change format of the list
            List<Boolean> newSignatures = new ArrayList<>();
            newSignatures.addAll(signatures);
            if (im_Master)
                newSignatures.add(false);
            else
                newSignatures.add(true);
            // -> continue petition, but build a new message with new nr.
            Delivery.Message.Builder messagePetition = MessageCreator.createMessageRoundOfSignatures(message.getRoundOfSignatures().getId(), newSignatures);
            inputStreamObserver.onNext(messagePetition.build());
        }
    }

    private void electedDroneLogic(Delivery.Message message) {
        printMessageReceived("message type 4 -> elected drone");
                /*
                    When a drone receives an elected message, it
                    - marks itself as a non participant
                    - forwards the message to its neighbor
                 */
        participant = false;
        if (message.getElected().getId() != id) {
            im_Master = false;
            master = message.getElected().getId();
            printSubLevelMessage("master id = " + master + " , I'm sending to it all my infos");
            inputStreamObserver.onNext(message);

            // -> send message to update the master
            Delivery.Message.Builder messageUpdateDroneMaster = MessageCreator.createMessageBatteryPositionStatus(id,
                    battery,
                    posX,
                    posY,
                    master,
                    totKilometers,
                    totDeliveries);
            inputStreamObserver.onNext(messageUpdateDroneMaster.build());

        } else {
            printSubLevelMessage("Everyone knows i'm the master");

            masterDroneInternalStructures.add(new MasterDroneInternalStructure(id,
                    battery,
                    posX,
                    posY,
                    true,
                    totKilometers,
                    totDeliveries));

            queueMqtt = new QueueMqtt(this);
            listenerMqtt = new ListenerMqtt(id, q, queueMqtt, inputStreamObserver);
            listenerMqtt.start();

            queueMqtt.updateInListOfDrones();
        }
    }

    private void infoForTheMasterLogic(Delivery.Message message) {
        printMessageReceived("message type 5 -> info for the master");
        // if the infos are for me
        if (message.getBatteryPositionStatus().getTo() == id) {
            boolean pass = checkIfDroneIsAlreadyInTheList(message.getBatteryPositionStatus().getId());
            if (pass) {
                addDroneToMasterList(
                        message.getBatteryPositionStatus().getId(),
                        message.getBatteryPositionStatus().getBattery(),
                        message.getBatteryPositionStatus().getPosX(),
                        message.getBatteryPositionStatus().getPosY(),
                        message.getBatteryPositionStatus().getTotKm(),
                        message.getBatteryPositionStatus().getTotDeliveries()
                );
            } else {
                printSubLevelMessage("Drone already in list of the master");
            }
            return;
        }

        // if message is not for me -> propagate
        inputStreamObserver.onNext(message);
    }

    /************************************************************************************************************************newPackage**/
    private void newPackageLogic(Delivery.Message message) {
        printMessageReceived("message type 6 -> Arrived a package");

        // the delivery is for this drone
        if (message.getDeliveryPackage().getReceiver() == id) {
            printSubLevelMessage("Package id = " + message.getDeliveryPackage().getIdPackage());
            printSubLevelMessage("Start the delivery");
            // -> package for this drone
            try {
                Thread.sleep(5000);
                battery -= 10;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printSubLevelMessage("DELIVERY COMPLETED");

            // -> compute km and position update --- detect PM data
            int collectionOfThePackageKm = computeKilometers(posX, posY, message.getDeliveryPackage().getPickUpX(), message.getDeliveryPackage().getPickUpY());

            int deliveryOfThePackageKm = computeKilometers(message.getDeliveryPackage().getPickUpX(),
                    message.getDeliveryPackage().getPickUpY(),
                    message.getDeliveryPackage().getDeliveryX(),
                    message.getDeliveryPackage().getDeliveryY());

            int totalKmFromLastDelivery = collectionOfThePackageKm + deliveryOfThePackageKm;
            totalKmFromLastDeliverySaved = totalKmFromLastDelivery;     // saving the parameter
            totKilometers += totalKmFromLastDelivery;
            totDeliveries += 1;
            posX = message.getDeliveryPackage().getDeliveryX();
            posY = message.getDeliveryPackage().getDeliveryY();
            measurementList.clear();
            measurementList.addAll(bufferSensorsPM10.readAllAndClean());

            printSubLevelMessage("Battery level = " + battery);
            printSubLevelMessage("Sending back info for the master");
            if (battery <= 15) {
                // the drone contact the other drones to exit
                printSubLevelMessage("battery < 15%, drone trying to exit");
                Delivery.Message.Builder exitMessage = MessageCreator.createExitMessageQuestion(-1, System.currentTimeMillis());
                q.putWithPriority(exitMessage.build());
            } else {
                Delivery.Message.Builder messageDeliveryCompleted = MessageCreator.createMessageDeliveryCompleted(id,
                        battery,
                        posX,
                        posY,
                        master,
                        System.currentTimeMillis(),
                        totalKmFromLastDelivery,
                        measurementList,
                        true);

                inputStreamObserver.onNext(messageDeliveryCompleted.build());
            }
            return;
        }

        // -> if none of the above, forward the message to the next drone
        inputStreamObserver.onNext(message);
    }

    private void ackDeliveryLogic(Delivery.Message message) {
        printMessageReceived("message type 7 -> ack from " + message.getDeliveryCompleted().getId());

        if (message.getDeliveryCompleted().getTo() != id) {
            // message not for me
            inputStreamObserver.onNext(message);
        } else {
            // translate from Delivery.Measurement in Measurement
            List<Delivery.Measurement> measurements = message.getDeliveryCompleted().getMeasurementList();
            List<Measurement> measurements1 = new ArrayList<>();
            measurements.forEach(element -> measurements1.add(new Measurement(element.getId(),
                    element.getType(),
                    element.getValue(),
                    element.getTimestamp())));

            // -> update list of the master with the new infos
            // -> remove the drone from the master list if it has finished his job
            if (message.getDeliveryCompleted().getContinue()) {
                updateDroneInMasterList(
                        message.getDeliveryCompleted().getId(),
                        message.getDeliveryCompleted().getBattery(),
                        message.getDeliveryCompleted().getPosX(),
                        message.getDeliveryCompleted().getPosY(),
                        true,
                        message.getDeliveryCompleted().getKmTraveled(),
                        measurements1);
                queueMqtt.updateInListOfDrones(); // -> a new drone is available for a delivery
            } else {
                printSubLevelMessage("Removing drone id = " + message.getDeliveryCompleted().getId() + " from master list");
                removeDroneFromMasterList(message.getDeliveryCompleted().getId());
            }
        }
    }

    /************************************************************************************************************************recharge**/
    private void manageQuestionOfRecharge(Delivery.Message message) {
        printMessageReceived("message type 8 -> ask for recharge");

        // case nr 1 -> this drone is alone in the ring so it can enter into it
        if (listDronesMapped.size() == 1) {
            takingResource();
            return;
        }

        // case nr 2 -> ask to everyone
        if (message.getRechargeMessage().getId() == -1) {
            // set variables for the drone
            stateForRecharge = StateForRecharge.WANTED;
            timestampForRecharge = message.getRechargeMessage().getTimestamp();
            localQueueForRecharge.clear();
            mapIdBooleanDronesForRecharge.clear();
            listDronesMapped.forEach(droneMapped -> {
                if (droneMapped.getId() != id)
                    mapIdBooleanDronesForRecharge.add(new MapIdBoolean(droneMapped.getId(), false));
            });

            // print the list of drones mapped
            for (int i = 0; i < mapIdBooleanDronesForRecharge.size(); i++)
                System.out.println(mapIdBooleanDronesForRecharge.get(i).getId() + " " + mapIdBooleanDronesForRecharge.get(i).getCheck());

            // create new message with this id then send it to everyone
            Delivery.Message.Builder rechargeMessage = MessageCreator.createRechargeMessageAsk(id, message.getRechargeMessage().getTimestamp());
            channelDirects.stream().parallel().forEach(element -> {
                if (element.getId() != id)
                    element.sendMessage(rechargeMessage.build());
            });

            return;
        }


        // case nr 3 -> compute => the drone will continue only when it will be ready
        if (message.getRechargeMessage().getId() != -1) {

            System.out.println("State for recharge = " + stateForRecharge);
            System.out.println("timestamp of this drone = " + timestampForRecharge);
            System.out.println("timestamp of the message arrived = " + message.getRechargeMessage().getTimestamp());
            // compare the timestamp of this drone with the one just arrived
            if (stateForRecharge == StateForRecharge.WANTED && timestampForRecharge < message.getRechargeMessage().getTimestamp()) {
                // the drone is waiting for the resource with timestamp inferior -> put the message in queue
                printSubLevelMessage("Drone already occupied with timestamp ahead. Put message in queue");
                localQueueForRecharge.add(message);
            } else {
                // it can answer
                composeAnswerRecharge(message);
            }
            return;
        }
    }

    private void manageAnswerOfRecharge(Delivery.Message message) {
        printMessageReceived("message type 9 -> answer for recharge");

        // setting the id of the drone to true because it has been obtained the answer
        int ID = message.getAnswerRechargeMessage().getId();
        checkTrueMapIdBooleanDrones(ID, mapIdBooleanDronesForRecharge);

        // print the list of drones mapped
        for (int i = 0; i < mapIdBooleanDronesForRecharge.size(); i++)
            System.out.println(mapIdBooleanDronesForRecharge.get(i).getId() + " " + mapIdBooleanDronesForRecharge.get(i).getCheck());


        // check if it has been received all the answers
        boolean stopper = false;
        for (int i = 0; i < mapIdBooleanDronesForRecharge.size(); i++)
            if (mapIdBooleanDronesForRecharge.get(i).getCheck() == false) {
                stopper = true;
                break;
            }

        if (stopper)
            return;

        // -> here managing the updating battery of the drone -> simulate with 10 seconds of delay
        takingResource();

        // updating position of the drone (0,0)
        posX = 0;
        posY = 0;

        // finally the resource is free, updating the other drones
        stateForRecharge = StateForRecharge.FREE;
        timestampForRecharge = Long.MAX_VALUE;
        mapIdBooleanDronesForRecharge.clear();
        while (localQueueForRecharge.size() > 0) {
            Delivery.Message savedMessage = (Delivery.Message) localQueueForRecharge.poll();
            printSubLevelMessage("Free the resource for drone id = " + savedMessage.getRechargeMessage().getId());
            composeAnswerRecharge(savedMessage);
        }
    }

    /************************************************************************************************************************exitNotControlled**/
    private void manageAreYouOkFunc(Delivery.Message message) {
        printMessageReceived("message type 10 -> drone id " + message.getAreYouOk().getId() + " asked me if i'm ok");
        printSubLevelMessage("I'm confirming");

        composeAnswerImOk(message);
    }

    private void manageImOkFunc(Delivery.Message message) {
        printMessageReceived("message type 11 -> answer from drone id = " + message.getImOk().getId());
    }

    /******************************************************************************************************************resetInternalStructure**/
    private void resetMasterDroneInternalStructure(Delivery.Message message) {
        printMessageReceived("message type 12 -> reset master internal structure");

        // i'm the master
        if (message.getResetInternalStructure().getId() == id) {
            // recompile master drone internal list
            recompileListMasterDroneInternalStructure();
            // reset all the status of the drones
            for (MasterDroneInternalStructure m : masterDroneInternalStructures)
                setStatusDrone(m.getId(), true);

            return;
        }

        // i'm not the master so the message is not for me
        inputStreamObserver.onNext(message);
    }

    /************************************************************************************************************************ControlledExit**/
    private void manageQuestionOfExit(Delivery.Message message) {
        printMessageReceived("message type 13 -> ask for exit");

        // case nr 1 -> this drone is alone in the ring so it can enter into it
        if (listDronesMapped.size() == 1) {
            drone_rpc.stopDroneClientServer();
            return;
        }

        // case nr 2 -> ask to everyone
        if (message.getExitMessage().getId() == -1) {

            // FIRST -> send last infos to the master
            Delivery.Message.Builder messageDeliveryCompleted = MessageCreator.createMessageDeliveryCompleted(id,
                    battery,
                    posX,
                    posY,
                    master,
                    System.currentTimeMillis(),
                    totalKmFromLastDeliverySaved,
                    measurementList,
                    false);

            // update the Listener
            channelDirects.forEach(element -> {
                if (element.getId() == master)
                    element.sendMessage(messageDeliveryCompleted.build());
            });

            // in the ring
            // inputStreamObserver.onNext(messageDeliveryCompleted.build()); -

            // set variables for the drone
            stateForExit = StateForExit.WANTED;
            timestampForExit = message.getExitMessage().getTimestamp();
            localQueueForExit.clear();
            mapIdBooleanDronesForExit.clear();
            listDronesMapped.forEach(droneMapped -> {
                if (droneMapped.getId() != id)
                    mapIdBooleanDronesForExit.add(new MapIdBoolean(droneMapped.getId(), false));
            });

            // SECOND -> print the list of drones mapped
            for (int i = 0; i < mapIdBooleanDronesForExit.size(); i++)
                System.out.println(mapIdBooleanDronesForExit.get(i).getId() + " " + mapIdBooleanDronesForExit.get(i).getCheck());

            // THIRD -> create new message with this id then send it to everyone
            Delivery.Message.Builder exitMessage = MessageCreator.createExitMessageQuestion(id, message.getExitMessage().getTimestamp());
            channelDirects.stream().parallel().forEach(element -> {
                if (element.getId() != id)
                    element.sendMessage(exitMessage.build());
            });

            return;
        }


        // case nr 3 -> compute => the drone will continue only when it will be ready
        if (message.getExitMessage().getId() != -1) {

            // compare the timestamp of this drone with the one just arrived
            System.out.println("State for exit = " + stateForExit);
            System.out.println("timestamp of this drone = " + timestampForExit);
            System.out.println("timestamp of the message arrived = " + message.getExitMessage().getTimestamp());
            if (stateForExit == StateForExit.WANTED && timestampForExit < message.getExitMessage().getTimestamp()) {
                // the drone is waiting for the resource with timestamp inferior -> put the message in queue
                printSubLevelMessage("Drone already occupied with timestamp ahead. Put message in queue");
                localQueueForExit.add(message);
            } else {
                // it can answer
                composeAnswerExit(message);
            }
            return;
        }
    }

    private void manageAnswerOfExit(Delivery.Message message) {
        printMessageReceived("message type 14 -> answer for exit");

        // setting the id of the drone to true because it has been obtained the answer
        int ID = message.getAnswerExitMessage().getId();
        checkTrueMapIdBooleanDrones(ID, mapIdBooleanDronesForExit);

        // print the list of drones mapped
        for (int i = 0; i < mapIdBooleanDronesForExit.size(); i++)
            System.out.println(mapIdBooleanDronesForExit.get(i).getId() + " " + mapIdBooleanDronesForExit.get(i).getCheck());


        // check if it has been received all the answers
        boolean stopper = false;
        for (int i = 0; i < mapIdBooleanDronesForExit.size(); i++)
            if (mapIdBooleanDronesForExit.get(i).getCheck() == false) {
                stopper = true;
                break;
            }

        if (stopper)
            return;

        // -> here managing the updating battery of the drone -> simulate with 10 seconds of delay
        drone_rpc.stopDroneClientServer();
    }

    /************************************************************************************************************************pendingOrders**/
    private void managePendingOrders(Delivery.Message message) {
        printMessageReceived("message type 15 -> manage pending orders");
        if (im_Master && queueMqtt != null) {
            List<Delivery.DeliveryPackage> arrayList = message.getOrderPending().getArrayList();
            for (Delivery.DeliveryPackage pack : arrayList) {
                printSubLevelMessage("Collect new package" + pack.getIdPackage());
                queueMqtt.put(new Order(pack.getIdPackage(),
                        pack.getPickUpX(),
                        pack.getPickUpY(),
                        pack.getDeliveryX(),
                        pack.getDeliveryY()));
            }
            return;
        }

        inputStreamObserver.onNext(message);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /******************************** -> MASTERDRONEINTERNALSTRUCTURE <- ******************************/

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public int chooseDroneForDelivery(List<MasterDroneInternalStructure> listDroneAvailable, int pickUpX, int pickUpY) {
        // select the nearest drone for picking up the package
        // if more than one is available with same distance, choose the one with more battery
        // if the battery is also the same, select the one with id bigger

        int id_ = -1;
        int battery = -1;
        double previousDistance = -1;

        // printing drones available
        /*
        for (int i = 0; i < listDroneAvailable.size(); i++)
            printSubLowLevelMessage("Id = " + listDroneAvailable.get(i).getId());

         */

        // check the distance -> check the battery -> check the id
        for (MasterDroneInternalStructure element : listDroneAvailable) {
            // -> compute distance
            double distance = computeKilometers(element.getPosX(),
                    element.getPosY(),
                    pickUpX,
                    pickUpY);
            // double distance = Math.sqrt(Math.pow(element.getPosX() - pickUpX, 2) + Math.pow(element.getPosY() - pickUpY, 2));
            if (distance > previousDistance && element.getStatus()) {
                battery = element.getBattery();
                previousDistance = distance;
                id_ = element.getId();
            } else if (distance == previousDistance && element.getStatus()) {

                // check battery
                if (battery < element.getBattery()) {
                    battery = element.getBattery();
                    id_ = element.getId();
                } else if (battery == element.getBattery()) {

                    // check id
                    if (id_ < element.getId())
                        id_ = element.getId();
                }
            }
        }

        setStatusDrone(id_, false);
        return id_;
    }

    private void removeDroneFromMasterList(int receiver) {
        synchronized (lockInternalStructureMaster) {
            masterDroneInternalStructures.removeIf(element -> element.getId() == receiver);
        }
    }

    private void addDroneToMasterList(int id, int battery, int posX, int posY, int totKm, int totDeliveries) {
        boolean check = false;
        synchronized (lockInternalStructureMaster) {
            for (MasterDroneInternalStructure element : masterDroneInternalStructures) {
                if (element.getId() == id) {
                    check = true;
                    break;
                }
            }
            if (!check) {
                printSubLevelMessage("new infos from drone with id " + id);
                masterDroneInternalStructures.add(new MasterDroneInternalStructure(
                        id,
                        battery,
                        posX,
                        posY,
                        true,
                        totKm,
                        totDeliveries
                ));
            } else {
                printSubLevelMessage("Drone id = " + id + " already in the list");
            }
        }
    }

    private void updateDroneInMasterList(int idDroneToUpdate, int newBattery, int newPosX, int newPosY, boolean newStatus, int kmTravelled, List<Measurement> measurements) {
        synchronized (lockInternalStructureMaster) {
            for (MasterDroneInternalStructure element : masterDroneInternalStructures) {
                if (element.getId() == idDroneToUpdate) {
                    element.setBattery(newBattery);
                    element.setPosX(newPosX);
                    element.setPosY(newPosY);
                    element.setStatus(newStatus);
                    element.setTotKM(element.getTotKM() + kmTravelled);
                    element.setNrDeliveries(element.getNrDeliveries() + 1);
                    measurements.forEach(c -> element.getMeasurementList().add(new Measurement(c.getId(),
                            c.getType(),
                            c.getValue(),
                            c.getTimestamp())));
                }
            }
        }
    }

    public void setStatusDrone(int id_, boolean status) {
        synchronized (lockInternalStructureMaster) {
            for (MasterDroneInternalStructure element : masterDroneInternalStructures)
                if (element.getId() == id_)
                    element.setStatus(status);
        }
    }

    public List<MasterDroneInternalStructure> checkDronesFree() {
        List<MasterDroneInternalStructure> listOfFreeDrones = new ArrayList<>();
        synchronized (lockInternalStructureMaster) {
            for (MasterDroneInternalStructure element : masterDroneInternalStructures)
                if (element.getStatus()) {      // if the drone is free then send it to the computing algorithm
                    listOfFreeDrones.add(element);
                }
        }
        return listOfFreeDrones;
    }

    private void recompileListMasterDroneInternalStructure() {
        synchronized (lockInternalStructureMaster) {
            printSubLevelMessage("Checking master drone internal list");
            List<MasterDroneInternalStructure> toDelete = new ArrayList<>();
            for (MasterDroneInternalStructure elementInInternalStructureOfMaster : masterDroneInternalStructures) {
                boolean found = false;

                internal_cycle:
                for (MapIdPort mapIdPort : listDronesMapped) {
                    if (elementInInternalStructureOfMaster.getId() == mapIdPort.getId()) {
                        found = true;
                        break internal_cycle;
                    }
                }

                // if it not found -> delete from master drone internal structure
                if (!found) {
                    toDelete.add(elementInInternalStructureOfMaster);
                }
            }

            // delete elements
            for (MasterDroneInternalStructure e : toDelete) {
                masterDroneInternalStructures.removeIf(element -> element.getId() == e.getId());
                printSubLowLevelMessage("Removing element " + e.getId() + " from master internal list");
            }
        }
    }

    private boolean checkIfDroneIsAlreadyInTheList(int id_) {
        synchronized (lockInternalStructureMaster) {
            for (MasterDroneInternalStructure element : masterDroneInternalStructures)
                if (element.getId() == id_)
                    return false;
        }
        return true;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> SETUP <- /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private void setupRing() {
        // setup ring connection
        this.channel = ManagedChannelBuilder.forAddress(ip, portNextDrone)
                .usePlaintext()
                .build();
        this.clientStub = DeliveryServiceGrpc.newStub(channel);
        inputStreamObserver = this.clientStub.delivery(new OutputStreamObserverRing());
        System.out.println();
        System.out.println("==== " + "Client connected successfully on port of the ring = " + portNextDrone + " ====");
        System.out.println();
        logger.log(Level.INFO, "Client connected successfully on port of the ring " + portNextDrone);
    }

    private void setupDirect() {
        // setup direct connection
        for (MapIdPort element : listDronesMapped) {
            channelDirects.add(new ChannelsDirect(ip, element.getId(), element.getPort()));
            System.out.println(">>>> " + "Connection established with drone id = " + element.getId() + " - port = " + element.getPort() + "<<<<");
        }
    }


    private void changePorts(Delivery.Message message) {
        // -> the timestamp is behind the timestamp of the package received
        if (timestamp < message.getTopologyMessage().getTimestamp()) {
            printSubLevelMessage("Timestamp behind, updating list...");
            // closing all the direct connection
            channelShutdownDirect();
            // -> new list incoming
            recompileListDroneMapped(message);
            // close all the direct connections
            channelShutdownDirect();
            // -> setup the direct connections, every time a drone enter or exit from the network it needs to recalculate the connections
            setupDirect();
            // -> update the timestamp
            timestamp = message.getTopologyMessage().getTimestamp();

            // -> recover the new port
            int newPort = Utils.findNextPortClient(id, listDronesMapped);
            // -> check if the port is changed
            if (!Utils.checkPorts(newPort, portNextDrone)) {
                printSubLevelMessage("Port is changed, now at -> " + newPort);
                // inputStreamObserver.onCompleted();
                channelShutdownRing();
                // -> select the new port
                portNextDrone = newPort;
                // -> setup
                setupRing();

            } else {
                printSubLevelMessage("Port is not changed");
            }
        }
    }

    private void recompileListDroneMapped(Delivery.Message message) {
        // -> clear the container of mapped drones
        listDronesMapped.clear();
        // -> recover the new list
        List<Delivery.TupleIdPort> infoList = message.getTopologyMessage().getArrayList();
        // -> copy the new list in our container
        for (Delivery.TupleIdPort element : infoList) {
            listDronesMapped.add(new MapIdPort(element.getId(), element.getPort()));
        }
        // -> sort the elements in the container
        Utils.sortMapListDronesById(listDronesMapped);
    }

    private void channelShutdownRing() {
        channel.shutdown();
    }

    private void channelShutdownDirect() {
        channelDirects.forEach(element -> {
            element.shutDown();
        });
        channelDirects.clear();
    }

    // WARNING -> this is executed by the thread father
    public void stopDrone() throws InterruptedException {

        // if i'm master, stop the listenerMqtt
        if (im_Master) {
            try {
                listenerMqtt.stopListening();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

        // stop simulator PM10
        pm10Simulator.stopMeGently();
        // stop printer
        printerStatistics.stopGentlyPrinter();

        List<MapIdPort> list = buildNewListDroneMapped(id);
        // put the new topology in queue
        Delivery.Message.Builder msg1 = MessageCreator.createMessageTopology(-1,
                System.currentTimeMillis(),
                list,
                false);
        q.put(msg1.build());
        // qWithPriority.put(msg1.build()); ?????

        // wait until the client is stopped
        synchronized (lockExit) {
            System.out.println("Wait for exit");
            lockExit.wait();
            System.out.println("Exiting...");
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> PRINT FUNCTIONS <- ///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private void printMessageReceived(String message) {
        System.out.println(++counterMessages + ". " + message);
    }

    private static void printSubLevelMessage(String message) {
        System.out.println("    " + ". " + message);
    }

    private static void printSubLowLevelMessage(String message) {
        System.out.println("    " + "   - " + message);
    }

    // https://currentmillis.com/tutorials/system-currentTimeMillis.html
    private String printTime(long currentTimeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTimeMillis);
        return calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND) + ":" + calendar.get(Calendar.MILLISECOND);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> USEFUL FUNCTIONS <- //////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private static int computeKilometers(int startX, int startY, int arrivalX, int arrivalY) {
        return (int) Math.sqrt(Math.pow(startX - arrivalX, 2) + Math.pow(startY - arrivalY, 2));
    }

    private boolean checkSignatures(List<Boolean> list) {
        boolean check = true;
        for (Boolean el : list)
            if (el == false)
                check = false;

        return check;
    }

    // answer of status of the resource
    private void composeAnswerRecharge(Delivery.Message message) {
        Delivery.Message.Builder answerMessage = MessageCreator.createRechargeMessageAnswer(id);
        channelDirects.forEach(element -> {
            if (element.getId() == message.getRechargeMessage().getId())
                element.sendMessage(answerMessage.build());
        });
    }

    // answer of status of the resource
    private void composeAnswerExit(Delivery.Message message) {
        Delivery.Message.Builder answerMessage = MessageCreator.createExitMessageAnswer(id);
        channelDirects.forEach(element -> {
            if (element.getId() == message.getExitMessage().getId())
                element.sendMessage(answerMessage.build());
        });
    }

    // answer to a vigilant drone
    private void composeAnswerImOk(Delivery.Message message) {
        Delivery.Message.Builder confirm = MessageCreator.createImOkMessageAnswer(id);
        channelDirects.forEach(element -> {
            if (element.getId() == message.getAreYouOk().getId())
                element.sendMessage(confirm.build());
        });
    }

    // take the resource and sleep
    private void takingResource() {
        try {
            System.out.println("********* ENTER CHARGING STATUS *********");
            Thread.sleep(10000);
            battery = 100;
            System.out.println("********* EXIT CHARGING STATUS *********");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // reset the state of the drone
    private void setStateOfVigilance() {
        stateOfVigilance = StateOfVigilance.CALM;
    }

    // create new list and exclude the ID passed
    private List<MapIdPort> buildNewListDroneMapped(int ID) {
        // building a new List = same List - this drone
        List<MapIdPort> newList = new ArrayList<>();
        for (MapIdPort element : listDronesMapped)
            if (!(element.getId() == ID))
                newList.add(element);

        return newList;
    }

    private void checkTrueMapIdBooleanDrones(int ID, List<MapIdBoolean> map) {
        for (int i = 0; i < map.size(); i++) {
            if (map.get(i).getId() == ID)
                map.get(i).setCheck(true);
        }
    }

    // return the size of the actual ring
    public static int sizeRing() {
        return listDronesMapped.size();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> SETTER / GETTER <- ///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public List<MapIdPort> getListDronesMapped() {
        return listDronesMapped;
    }

    /*
        private boolean im_master
        private int battery
        private int totDeliveries
        private int totKilometers
     */
    public boolean getIm_Master() {
        return im_Master;
    }

    public int getTotDeliveries() {
        return totDeliveries;
    }

    public int getTotKilometers() {
        return totKilometers;
    }

    public int getBattery() {
        return battery;
    }

    // WARNING -> call this method if and only if who calls it checked of being the master
    public synchronized List<MasterDroneInternalStructure> getMasterDroneInternalStructures() {
        return masterDroneInternalStructures;
    }
}




