package DRONE;

import BEANS.Drone;
import UTILS.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static DRONE.RequestServer.deleteRequest;
import static DRONE.RequestServer.postRequest;

public class Main extends Thread {

    private static List<Drone> listDrones;
    private static int posX;
    private static int posY;
    private static long timestamp;

    public static void main(String[] args){

        // starting drone
        /*
            > ID
            > port for communication between drones
            > address of the server administrator
         */

        Client client = Client.create();
        int ID = Utils.generateID();

        // ip and port for the server
        String ip = "localhost";
        int portDrone = Utils.findFreePort();


        // port and string for the server administrator
        int portServer = 6789;
        String serverAddressAdministrator = "http://" + ip + ":" + portServer;

        // request for the drone
        String postPathAdd = "/drone/add";
        String postPathDelete = "/drone/delete";

        Drone drone = new Drone(ID, ip, portDrone);
        ClientResponse clientResponseAdd = postRequest(client, serverAddressAdministrator + postPathAdd, drone);
        if (clientResponseAdd.getStatus() == 200){
            System.out.println(">>> Drone added to the list >>>");
            System.out.println(">>> Loading the list of drones >>>");

            String listDronesStrings = clientResponseAdd.getEntity(String.class);

            Type listOfMyClassObject = new TypeToken<ArrayList<Drone>>() {}.getType();
            Gson gson = new Gson();
            listDrones = gson.fromJson(listDronesStrings, listOfMyClassObject);

            // we obtain directly the starting position of the drone cycling on all drones
            // when we check our drone we take the position
            for (Drone d : listDrones){
                if (d.getId() == ID){
                    posX = d.getPosX();
                    posY = d.getPosY();
                    timestamp = d.getMillis();
                }
            }

        } else {
            System.out.println("Id drone already in use, retry");
            System.exit(1);
        }


        // if everything worked correctly we can try to insert the drone into the ring
        Drone_rpc drone_rpc = new Drone_rpc(ID, ip, portDrone, posX, posY, listDrones, timestamp);
        drone_rpc.start();
        try {
            // wait until the drone want to exit
            drone_rpc.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ClientResponse clientResponseDelete = deleteRequest(client, serverAddressAdministrator + postPathDelete, drone);
        switch (clientResponseDelete.getStatus()) {
            case 200:
                System.out.println(">>> Drone deleted from the list >>>");
                break;
            case 409:
                System.out.println("ERROR -> it was not possible to delete the drone");
                break;
            case 404:
                System.out.println("ERROR -> Drone not found in the list");
                break;
            default:
                System.out.println("ERROR -> Some error occurred in the server");
                break;
        }

        client.destroy();

        Utils.printClosingThread("MAIN");
        System.exit(0);
    }
}
