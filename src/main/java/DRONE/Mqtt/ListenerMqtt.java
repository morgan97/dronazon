package DRONE.Mqtt;

// bibliography
// https://bytesofgigabytes.com/mqtt/java-as-mqtt-publisher-and-subscriber-client/

import BEANS.Order;
import DRONE.Delivery;
import DRONE.Queue;
import UTILS.Utils;
import com.google.gson.Gson;
import io.grpc.stub.StreamObserver;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

public class ListenerMqtt extends Thread implements MqttCallback {
    /**
     * The broker url.
     */
    private static final String brokerUrl = "tcp://localhost:1883";

    /**
     * The client id.
     */
    private static final String clientId = "clientId";

    /**
     * The topic.
     */
    private static final String topic = "dronazon/smartcity/orders";

    protected int id;
    protected Queue q;
    private MqttClient sampleClient;
    private StreamObserver<Delivery.Message> inputStreamObserver;
    private volatile boolean listening = true;
    private QueueMqtt queueMqtt;

    public ListenerMqtt(int id, Queue q, QueueMqtt queueMqtt, StreamObserver<Delivery.Message> inputStreamObserver) {
        this.id = id;
        this.q = q;
        this.inputStreamObserver = inputStreamObserver;
        this.queueMqtt = queueMqtt;
    }

    @Override
    public void run() {
        //	logger file name and pattern to log
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            sampleClient = new MqttClient(brokerUrl, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            System.out.println("checking");
            System.out.println("Mqtt Connecting to broker: " + brokerUrl);

            sampleClient.connect(connOpts);
            System.out.println("Mqtt Connected");

            sampleClient.setCallback(this);
            sampleClient.subscribe(topic);

            System.out.println("Subscribed");
            System.out.println("Listening");



            while (listening) {
                queueMqtt.takeOrders(q, id);
            }

        } catch (MqttException me) {
            System.out.println("Impossible to connect on the topic -> " + topic);
        } finally {
            Utils.printClosingThread("LISTENER MQTT");
        }
    }

    public void stopListening() throws MqttException {
        listening = false;
        queueMqtt.updateInListOfDrones();
        sampleClient.disconnect();
    }

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        byte[] payload = message.getPayload();
        String s = new String(payload, StandardCharsets.UTF_8);
        Gson g = new Gson();
        // -> unboxing
        Order order = g.fromJson(s, Order.class);
        queueMqtt.put(order);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}

