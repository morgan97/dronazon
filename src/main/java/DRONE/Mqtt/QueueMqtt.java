package DRONE.Mqtt;

import BEANS.MasterDroneInternalStructure;
import BEANS.Order;
import DRONE.Delivery;
import DRONE.GrpcClient;
import DRONE.MessageCreator;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class QueueMqtt {

    private GrpcClient grpcClient;

    public QueueMqtt(GrpcClient grpcClient) {
        this.grpcClient = grpcClient;
    }

    private Queue<Order> buffer = new LinkedList<>();

    public synchronized void put(Order order) {
        buffer.add(order);
        this.notify();
    }

    public synchronized Order take() {
        return buffer.poll();
    }

    public synchronized boolean dimension() {
        return buffer.size() > 0;
    }

    public synchronized void takeOrders(DRONE.Queue q, int id) {
        List<MasterDroneInternalStructure> freeDrones = grpcClient.checkDronesFree();
        boolean dimension = dimension();
        /*
        for (int i = 0; i < freeDrones.size(); i++)
            System.out.println("id libero = " + freeDrones.get(i).getId());
        System.out.println(dimension);
         */

        if (freeDrones.size() > 0 && dimension) {
            Order order = take();

            // select the drone who sends the package
            int chosenID = grpcClient.chooseDroneForDelivery(freeDrones, order.getPick_upX(), order.getPick_upY());

            // -> put a message in the main queue for the master to deliver a new package
            Delivery.Message.Builder messageDeliveryPackage = MessageCreator.createMessageDeliveryPackage(id,
                    chosenID,
                    order.getIdPackage(),
                    order.getPick_upX(),
                    order.getPick_upY(),
                    order.getDeliveryX(),
                    order.getDeliveryY());
            q.put(messageDeliveryPackage.build());


        } else {
            try {
                this.wait();
            } catch (InterruptedException e) {
                System.out.println("entered");
                e.printStackTrace();
            }
        }
    }

    public synchronized void updateInListOfDrones() {
        this.notify();
    }

    public Queue<Order> getAllTheDeliveriesPending() {
        return buffer;
    }
}
