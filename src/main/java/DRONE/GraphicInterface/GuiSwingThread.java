package DRONE.GraphicInterface;

// bibliography
// -> https://www.geeksforgeeks.org/java-swing-simple-user-registration-form/

import BEANS.MasterDroneInternalStructure;
import DRONE.Delivery;
import DRONE.MessageCreator;
import DRONE.Queue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

// Driver Code
public class GuiSwingThread implements Runnable {
    private Queue queue;
    private MyFrame f = null;

    public GuiSwingThread(Queue q) {
        this.queue = q;
    }

    @Override
    public void run() {
        f = new MyFrame(queue);
    }

    public void reloadInfoGUI(int id, int battery, int posX, int posY, boolean im_master, List<MasterDroneInternalStructure> masterDroneInternalStructures) {
        f.updateInfo(id, battery, posX, posY, im_master, masterDroneInternalStructures);
    }

    public void addStatistics(String dataToPrint) {
        f.updateStatistics(dataToPrint);
    }
}

class MyFrame extends JFrame implements ActionListener {

    // Components of the Form
    private Container c;
    private JLabel title;
    private JProgressBar jProgressBar;
    private JLabel battery;
    private JLabel position;
    private JLabel positionX;
    private JLabel positionY;
    private JLabel imMaster;
    private JLabel masterInternalStructure;
    private JButton buttonRecharge;
    private JButton buttonExit;

    // textArea masterDroneInternalStructure
    private JTextArea masterInternalStructureArea;
    private JScrollPane jScrollPaneMasterInternalStructure;

    // textArea statisticsArea
    private JTextArea statisticsArea;
    private JScrollPane jScrollPaneStatisticsArea;

    // constructor, to initialize the components
    // with default values.
    public MyFrame(Queue queue) {
        setTitle("Drone");
        setBounds(100, 90, 550, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);

        c = getContentPane();
        c.setLayout(null);

        title = new JLabel("Drone Id = ");
        title.setFont(new Font("Arial", Font.PLAIN, 25));
        title.setSize(300, 30);
        title.setLocation(20, 30);
        c.add(title);

        battery = new JLabel("Battery = ");
        battery.setFont(new Font("Arial", Font.PLAIN, 20));
        battery.setSize(300, 20);
        battery.setLocation(20, 100);
        c.add(battery);

        jProgressBar = new JProgressBar(0, 100);
        jProgressBar.setBounds(20, 130, 200, 100);
        jProgressBar.setSize(200, 30);
        jProgressBar.setValue(0);
        c.add(jProgressBar);

        position = new JLabel("Position");
        position.setFont(new Font("Arial", Font.PLAIN, 20));
        position.setSize(100, 20);
        position.setLocation(20, 160);
        c.add(position);

        positionX = new JLabel("X = ");
        positionX.setFont(new Font("Arial", Font.PLAIN, 15));
        positionX.setSize(80, 20);
        positionX.setLocation(130, 160);
        c.add(positionX);

        positionY = new JLabel("Y = ");
        positionY.setFont(new Font("Arial", Font.PLAIN, 15));
        positionY.setSize(80, 20);
        positionY.setLocation(180, 160);
        c.add(positionY);

        imMaster = new JLabel("I'm master = ");
        imMaster.setFont(new Font("Arial", Font.BOLD, 20));
        imMaster.setSize(200, 20);
        imMaster.setLocation(20, 190);
        c.add(imMaster);

        masterInternalStructure = new JLabel("MasterInternalStructure");
        masterInternalStructure.setFont(new Font("Arial", Font.PLAIN, 20));
        masterInternalStructure.setSize(230, 20);
        masterInternalStructure.setLocation(20, 220);
        c.add(masterInternalStructure);

        /*************************** area internal drone structure *****************************/
        masterInternalStructureArea = new JTextArea();
        masterInternalStructureArea.setFont(new Font("Arial", Font.PLAIN, 15));
        // masterInternalStructureArea.setSize(200, 120);
        masterInternalStructureArea.setLineWrap(true);
        masterInternalStructureArea.setEditable(false);

        jScrollPaneMasterInternalStructure = new JScrollPane(masterInternalStructureArea);
        jScrollPaneMasterInternalStructure.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPaneMasterInternalStructure.setSize(220, 180);
        jScrollPaneMasterInternalStructure.setLocation(20, 250);
        c.add(jScrollPaneMasterInternalStructure);

        /*************************** area statistics ******************************************/
        statisticsArea = new JTextArea();
        statisticsArea.setFont(new Font("Arial", Font.PLAIN, 15));
        // statisticsArea.setSize(250, 380);
        statisticsArea.setLineWrap(true);
        statisticsArea.setEditable(false);

        jScrollPaneStatisticsArea = new JScrollPane(statisticsArea);
        jScrollPaneStatisticsArea.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPaneStatisticsArea.setSize(250, 380);
        jScrollPaneStatisticsArea.setLocation(250, 50);
        c.add(jScrollPaneStatisticsArea);

        /*************************** buttons -> Recharge | Exit *******************************s*/
        buttonRecharge = new JButton("Recharge");
        buttonRecharge.setSize(90, 25);
        buttonRecharge.setLocation(160, 440);
        buttonRecharge.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                Delivery.Message.Builder rechargeMessage = MessageCreator.createRechargeMessageAsk(-1, System.currentTimeMillis());
                queue.putWithPriority(rechargeMessage.build());
            }
        });
        c.add(buttonRecharge);

        buttonExit = new JButton("Exit");
        buttonExit.setSize(90, 25);
        buttonExit.setLocation(270, 440);
        buttonExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                long time = System.currentTimeMillis();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                Delivery.Message.Builder exitMessage = MessageCreator.createExitMessageQuestion(-1, time);
                queue.putWithPriority(exitMessage.build());
            }
        });
        c.add(buttonExit);

        setVisible(true);
    }

    // method actionPerformed()
    // to get the action performed
    // by the user and act accordingly
    public void actionPerformed(ActionEvent e) {

    }

    public void updateInfo(int id, int battery, int posX, int posY, boolean im_master, List<MasterDroneInternalStructure> masterDroneInternalStructures) {
        this.title.setText("Drone Id = " + id);
        this.battery.setText("Battery = " + battery + "%");
        this.jProgressBar.setValue(battery);
        this.positionX.setText("X = " + posX);
        this.positionY.setText("Y = " + posY);
        this.imMaster.setText("I'm master = " + im_master);

        if (im_master) {
            String list = "";
            for (int i = 0; i < masterDroneInternalStructures.size(); i++) {
                boolean status = masterDroneInternalStructures.get(i).getStatus();

                if (status)
                    list = list + "id = " + masterDroneInternalStructures.get(i).getId() + " -> free \n";
                else
                    list = list + "id = " + masterDroneInternalStructures.get(i).getId() + " -> occupied  \n";

            }
            masterInternalStructureArea.setText(list);
        }
    }

    public void updateStatistics(String dataToPrint) {
        String update = dataToPrint + statisticsArea.getText();
        statisticsArea.setText(update);
    }
}

