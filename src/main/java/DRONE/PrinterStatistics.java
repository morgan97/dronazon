package DRONE;

import BEANS.MasterDroneInternalStructure;
import BEANS.StatisticAverage;
import DRONE.GraphicInterface.GuiSwingThread;
import SIMULATORI.Measurement;
import UTILS.Utils;
import com.google.gson.Gson;
import com.sun.jersey.api.client.*;

import java.util.ArrayList;
import java.util.List;

public class PrinterStatistics extends Thread {

    private volatile boolean continuePrinting = true;

    private GrpcClient grpcClient;
    private GuiSwingThread threadSwingInterface;
    private Client client;

    PrinterStatistics(GrpcClient grpcClient, GuiSwingThread threadSwingInterface) {
        this.grpcClient = grpcClient;
        this.threadSwingInterface = threadSwingInterface;
    }

    @Override
    public void run() {
        client = Client.create();

        // port and string for the server administrator
        String ip = "localhost";
        int portServer = 6789;
        String serverAddressAdministrator = "http://" + ip + ":" + portServer;

        // request for sending the info
        String postPathAdd = "/statistics/add";


        while (continuePrinting) {

            // print statistics about a drone
            String dataToPrint = "";
            dataToPrint += "TotDeliveries = " + grpcClient.getTotDeliveries() + "\n" +
                    "TotKm = " + grpcClient.getTotKilometers() + "\n" +
                    "battery = " + grpcClient.getBattery() + "\n" +
                    "----------------------------------" + "\n";

            threadSwingInterface.addStatistics(dataToPrint);


            if (grpcClient.getIm_Master()) {
                int sizeDroneInternalStructure = grpcClient.getMasterDroneInternalStructures().size();
                if (sizeDroneInternalStructure > 0) {
                    int totKMStat = 0;
                    int totNrDeliveriesStat = 0;
                    int meanBatteryStat = 0;
                    double meanPollution = 0;

                    // mean km
                    totKMStat = grpcClient.getMasterDroneInternalStructures().stream().mapToInt(MasterDroneInternalStructure::getTotKM).sum() / sizeDroneInternalStructure;
                    // mean deliveries
                    totNrDeliveriesStat = grpcClient.getMasterDroneInternalStructures().stream().mapToInt(MasterDroneInternalStructure::getNrDeliveries).sum() / sizeDroneInternalStructure;
                    // mean battery
                    meanBatteryStat = grpcClient.getMasterDroneInternalStructures().stream().mapToInt(MasterDroneInternalStructure::getBattery).sum() / sizeDroneInternalStructure;
                    // mean pollution
                    List<Double> totMeasurements = new ArrayList<>();
                    int counter = 0;
                    for (int i = 0; i < grpcClient.getMasterDroneInternalStructures().size(); i++) {
                        List<Measurement> current = grpcClient.getMasterDroneInternalStructures().get(i).getMeasurementList();
                        for (int j = 0; j < current.size(); j++) {
                            totMeasurements.add(current.get(i).getValue());
                            counter++;
                        }
                    }
                    if (totMeasurements.size() != 0)
                        meanPollution = totMeasurements.stream().mapToDouble(a -> a).sum() / counter;

                    // new timestamp
                    long time = System.currentTimeMillis();

                    // creation of a new Instance of the averages
                    StatisticAverage statisticAverage = new StatisticAverage(time, totKMStat, totNrDeliveriesStat, meanBatteryStat, meanPollution);
                    System.out.println("STAT = " + time + " : " + totKMStat + " : " + totNrDeliveriesStat + " : " + meanBatteryStat + " : " + meanPollution);

                    // send all the infos to the server
                    ClientResponse clientResponse = postRequest(client, serverAddressAdministrator + postPathAdd, statisticAverage);
                    // manage answer
                    if (clientResponse.getStatus() == 200) {
                        // System.out.println(">>> new statistics correctly added to the list >>>");
                    } else {
                        System.out.println("ERROR COD -> " + clientResponse.getStatus());
                        System.out.println("ERROR -> Some error occurred in the server");
                    }
                }
            }
            waitPrinter();
        }
        Utils.printClosingThread("PRINTER STATISTICS");
    }

    // wait ten second, then
    // -> print the statistics
    // -> if i'm master -> send data to the server
    private synchronized void waitPrinter() {
        // sleep ten seconds
        try {
            this.wait(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // WARNING -> the function is executed by the thread father
    // -> closing the printer
    public synchronized void stopGentlyPrinter() {
        continuePrinting = false;
        this.notify();
    }

    private static ClientResponse postRequest(Client client, String url, StatisticAverage statisticAverage) {
        WebResource webResource = client.resource(url);
        String input = new Gson().toJson(statisticAverage);
        // System.out.println(input);
        try {
            return webResource.type("application/json").post(ClientResponse.class, input);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            System.out.println("Impossible to reach the server, maybe Server went down");
            return null;
        }
    }

}
