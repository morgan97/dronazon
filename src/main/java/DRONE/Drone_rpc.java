package DRONE;

import BEANS.Drone;
import BEANS.MapIdPort;
import UTILS.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Drone_rpc extends Thread{
    private int id;
    private String ip;
    private int portDrone;
    private int posX;
    private int posY;
    private List<Drone> listDrones;
    private List<MapIdPort> listDroneMap;
    private long timestamp;
    private Queue queue = null;
    Logger logger = null;
    private GrpcClient grpcClient;
    private GrpcServer grpcServer;

    private final Object exitLock = new Object();


    public Drone_rpc(int id, String ip, int portDrone, int posX, int posY, List<Drone> listDrones, long timestamp) {
        this.id = id;
        this.ip = ip;
        this.portDrone = portDrone;
        this.posX = posX;
        this.posY = posY;
        this.listDrones = listDrones;
        this.timestamp = timestamp;
        this.queue = new Queue();                       // -> init queue
        logger = Logger.getLogger(Drone_rpc.class.getName());

        /////////////////////////////////////////////////////////////////
        Utils.sortListDronesById(listDrones);
        Utils.printListDrones(listDrones);              // -> print list
        /////////////////////////////////////////////////////////////////
    }

    @Override
    public void run() {
        // create the list ID + PORT and init new queue
        createMapDroneIdPort();
        Queue q = new Queue();

        // start the server side
        grpcServer = new GrpcServer(id, ip, portDrone, q, logger);
        grpcServer.start();

        // start the client side
        // find the port to connect the client
        int portClient = Utils.findNextPortClient(id, listDroneMap);
        System.out.println();
        System.out.println("DRONE ID = " + id + " PORT " + portDrone);
        System.out.println("GOES INTO ==> " + portClient);
        System.out.println();
        grpcClient = new GrpcClient(this, id, ip, portClient, portDrone, q, timestamp, listDroneMap, logger, posX, posY);
        grpcClient.start();

        // wait until event of exit happens
        synchronized (exitLock){
            try {
                exitLock.wait();

                grpcClient.stopDrone();
                grpcServer.stopServer();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Utils.printClosingThread("DRONE_RPC");

    }


    public void stopDroneClientServer(){
        synchronized (exitLock){
            exitLock.notify();
        }
    }


    private void createMapDroneIdPort() {
        listDroneMap = new ArrayList<>();
        for (Drone d : listDrones) {
            listDroneMap.add(new MapIdPort(d.getId(), d.getPortDrone()));
        }
        Utils.sortMapListDronesById(listDroneMap);
    }


}


