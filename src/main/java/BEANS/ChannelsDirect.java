package BEANS;

import DRONE.Delivery;

import DRONE.RechargeServiceGrpc;
import DRONE.StreamObservers.OutputStreamObserverDirect;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ChannelsDirect {

    private final String ip;
    private int id;
    private int port;
    private ManagedChannel channel;
    private RechargeServiceGrpc.RechargeServiceStub clientStub;
    private StreamObserver<Delivery.Message> inputStreamObserver;

    public ChannelsDirect(String ip, int id, int portToConnect) {
        this.ip = ip;
        this.id = id;
        this.port = portToConnect;

        this.channel = ManagedChannelBuilder.forAddress(ip, port)
                .usePlaintext()
                .build();
        this.clientStub = RechargeServiceGrpc.newStub(channel);
        inputStreamObserver = this.clientStub.recharge(new OutputStreamObserverDirect());
    }


    public void shutDown() {
        channel.shutdown();
    }

    public void sendMessage(Delivery.Message message){
        inputStreamObserver.onNext(message);
    }

    public StreamObserver<Delivery.Message> getInputStreamObserver() {
        return inputStreamObserver;
    }

    public int getId() {
        return id;
    }

    public int getPort() {
        return port;
    }
}
