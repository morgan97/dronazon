package BEANS;

public class Order {
    private int idPackage;
    private int pick_upX;
    private int pick_upY;
    private int deliveryX;
    private int deliveryY;

    public Order(int id, int pick_upX, int pick_upY, int deliveryX, int deliveryY){
        this.idPackage = id;
        this.pick_upX = pick_upX;
        this.pick_upY = pick_upY;
        this.deliveryX = deliveryX;
        this.deliveryY = deliveryY;
    }

    public int getPick_upX() {
        return pick_upX;
    }

    public int getPick_upY() {
        return pick_upY;
    }

    public int getDeliveryX() {
        return deliveryX;
    }

    public int getDeliveryY() {
        return deliveryY;
    }

    public int getIdPackage() {
        return idPackage;
    }
}

