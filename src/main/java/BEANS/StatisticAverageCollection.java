package BEANS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD)
public class StatisticAverageCollection {
    @XmlElement(name="StatisticAverage")
    private List<StatisticAverage> averageList;

    private static StatisticAverageCollection instance;

    private StatisticAverageCollection() {
        averageList = new ArrayList<>();
    }

    // singleton
    public synchronized static StatisticAverageCollection getInstance(){
        if (instance == null)
            instance = new StatisticAverageCollection();
        return instance;
    }

    // get the list of drones
    public synchronized List<StatisticAverage> getAverageList(){
        return new ArrayList<>(averageList);
    }

    // set the list of drones
    public synchronized void setAverageList(List<StatisticAverage> averageList){
        this.averageList = averageList;
    }

    // add a new drone to the list
    public synchronized void add(StatisticAverage d){
        averageList.add(d);
    }

}
