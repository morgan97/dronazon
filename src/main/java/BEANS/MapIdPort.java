package BEANS;

public class MapIdPort {
    private int id;
    private int port;

    public MapIdPort(int id, int port){
        this.id = id;
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
