package BEANS;

import javax.xml.bind.annotation.XmlRootElement;

// THE FOLLOWING CLASS CONTAINS THE INSTANCE OF AN AVERAGE COMPUTED BY THE MASTER DRONE

@XmlRootElement
public class StatisticAverage {
    private long timestamp;
    private int km;
    private int deliveries;
    private int battery;
    private double pollution;

    public StatisticAverage(){}

    public StatisticAverage(long timestamp, int km, int deliveries, int battery, double pollution){
        this.timestamp = timestamp;
        this.km = km;
        this.deliveries = deliveries;
        this.battery = battery;
        this.pollution = pollution;
    }


    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(int deliveries) {
        this.deliveries = deliveries;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public double getPollution() {
        return pollution;
    }

    public void setPollution(double pollution) {
        this.pollution = pollution;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
