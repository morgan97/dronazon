package UTILS;

import BEANS.Drone;
import BEANS.MapIdPort;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;

import static DRONE.RequestServer.deleteRequestId;

public class Utils {

    // https://stackoverflow.com/questions/51099027/find-free-port-in-java
    /*
        return a free port
     */
    public static int findFreePort() {
        int port = 0;
        // For ServerSocket port number 0 means that the port number is automatically allocated.
        try (ServerSocket socket = new ServerSocket(0)) {
            // Disable timeout and reuse address after closing the socket.
            socket.setReuseAddress(true);
            port = socket.getLocalPort();
        } catch (IOException ignored) {}
        if (port > 0) {
            return port;
        }
        throw new RuntimeException("Could not find a free port");
    }

    /*
        return the id of the drone
        the id is chosen in the range 0 - 99
     */
    public static int generateID() {
        Random rand = new Random();
        return rand.nextInt(100);
    }

    public static int generatePos(){
        Random rand = new Random();
        return rand.nextInt(10);
    }

    // find port of the client to connect from list
    public static int findNextPortClient(int id, List<MapIdPort> listDroneMap) {
        int index = -1;
        if (listDroneMap.size() == 1) {
            index = 0;
        } else {
            for (int i = 0; i < listDroneMap.size(); i++) {
                if (listDroneMap.get(i).getId() == id)
                    index = i;
            }
            // check if the drone is the last
            if (index == listDroneMap.size() - 1)
                index = 0;
            else
                index++;          // select the next drone
        }
        if (index == -1) throw new AssertionError();
        return listDroneMap.get(index).getPort();
    }

    // find port of the client to connect from list
    public static int findNextIdClient(int id, List<MapIdPort> listDroneMap) {
        int index = -1;
        if (listDroneMap.size() == 1) {
            index = 0;
        } else {
            for (int i = 0; i < listDroneMap.size(); i++) {
                if (listDroneMap.get(i).getId() == id)
                    index = i;
            }
            // check if the drone is the last
            if (index == listDroneMap.size() - 1)
                index = 0;
            else
                index++;          // select the next drone
        }
        if (index == -1) throw new AssertionError();
        return listDroneMap.get(index).getId();
    }

    // find port of the client to connect from list
    public static int findPredecessorIdClient(int id, List<MapIdPort> listDroneMap) {
        int index = -1;
        if (listDroneMap.size() == 1) {
            index = 0;
        } else {
            for (int i = 0; i < listDroneMap.size(); i++) {
                if (listDroneMap.get(i).getId() == id)
                    index = i;
            }
            // check if the drone is the first
            if (index == 0)
                index = listDroneMap.size() - 1;
            else
                index--;          // select the next drone
        }
        if (index == -1) throw new AssertionError();
        return listDroneMap.get(index).getId();
    }

    public static void sortListDronesById(List<Drone> listDrones) {
        listDrones.sort(Comparator.comparing(Drone::getId));
    }

    public static void sortMapListDronesById(List<MapIdPort> listDroneMap){
        listDroneMap.sort(Comparator.comparing(MapIdPort::getId));
    }

    public static void printListDrones(List<Drone> listDrones){
        for (Drone d : listDrones) {
            System.out.print("id -> " + d.getId());
            System.out.print(" port " + d.getPortDrone());
            System.out.print(" posX " + d.getPosX());
            System.out.print(" posY " + d.getPosY());
            System.out.print(" timestamp " + d.getMillis());
            System.out.println();
        }
    }

    public static boolean checkPorts(int a, int b){
        return a == b ? true : false;
    }

    public static long getNewTimestamp(){
        return System.currentTimeMillis();
    }


    public static int findPredecessorId(int value, List<MapIdPort> listDroneMap) {
        int index = -1;
        for (int i=0; i<listDroneMap.size(); i++){
            if (listDroneMap.get(i).getId() == value)
                index = i;
        }

        if (index == -1){
            // it means that it has not found itself
            // can happen sometimes when this function is called
            List<MapIdPort> copy = new ArrayList<>();
            copy.addAll(listDroneMap);
            copy.add(new MapIdPort(value, 0000));
            sortMapListDronesById(copy);

            for (int i=0; i<copy.size(); i++){
                if (copy.get(i).getId() == value)
                    index = i;
            }
        }

        if (index == 0)
            index = listDroneMap.size() - 1;
        else
            index--;

        return listDroneMap.get(index).getId();
    }

    public static int findMyPort(List<MapIdPort> list, int id){
        int port = -1;

        for (int i=0; i<list.size(); i++){
            if (list.get(i).getId() == id) {
                port = list.get(i).getPort();
            }
        }

        return port;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// -> REMOVE DRONE FROM SERVER <- //////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void removeDroneFromServer(int idBehind) {
        Client client = Client.create();
        // ip and port for the server
        String ip = "localhost";
        // port and string for the server administrator
        int portServer = 6789;
        String serverAddressAdministrator = "http://" + ip + ":" + portServer;
        String postPathDelete = "/drone/deleteId=/";

        String url = serverAddressAdministrator + postPathDelete + idBehind;
        System.out.println(url);
        ClientResponse clientResponseDelete = deleteRequestId(client, url);
        switch (clientResponseDelete.getStatus()) {
            case 200:
                System.out.println(">>> Drone deleted from the list >>>");
                break;
            case 409:
                System.out.println("ERROR -> it was not possible to delete the drone");
                break;
            case 404:
                System.out.println("ERROR -> Drone not found in the list");
                break;
            default:
                System.out.println("ERROR -> Some error occurred in the server");
                break;
        }

        client.destroy();
    }

    public static void printClosingThread(String s){
        System.out.println("****** CLOSING Thread -> " + s + "******");
    }

}
