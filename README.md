
# README #

[Link Repo](https://morgan97@bitbucket.org/morgan97/dronazon.git)

To build the project, import it in Intellij and select SDK 1.8

### RUN ###

The project is composed by 4 parts.
All the code is in the directory /src/main/java

Start running the mosquitto service
* brew service start mosquitto.

Then start the Server Administrator and the drones 
* Run ServerAdministrator.java
* Enter some drones from the directory DRONE/Main.java

Finally, start the publisher
* Run DRONAZON/MqttPublisher.java

To access data about drones and orders:
* Run CLIENT_AMM/ClientAdministrator.java

Each drone has a Graphical Interface of reference, all the other services are displayed in the terminal.

For stopping a drone is possible to use the buttons in the Gui or instead the command line
